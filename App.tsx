import { Providers } from 'providers/Providers';
import { AppRouteManager } from 'routes';

export default function App() {
  return (
    <Providers>
      <AppRouteManager />
    </Providers>
  );
}
