# Boilerplate GeBIT Expo

Este boilerplate possui:
```
- Configurações iniciais de navegação e bibliotecas
- Import alias
- Context Auth Inicial
- Eslint, prettier, husky
- SVG conversor
- Sombras
- Configuração de pastas
- Configuração base de API (Axios e React Query)
```

___
## OBS

Bibliotecas básicas de estilo não foram adicionadas, ver quais  se encaixam melhor para o projeto pensando na manutenção futura.

Exemplos:

  - [Native Base](https://docs.nativebase.io/)
  - [React Native Paper](https://reactnativepaper.com/)
  - [React Native Elements](https://reactnativeelements.com/)
  - [Native Wind](https://www.nativewind.dev/)
  - [Gluestack](https://gluestack.io/)


---
