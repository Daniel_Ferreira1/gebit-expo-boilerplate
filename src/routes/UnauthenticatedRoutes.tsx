import { NavigationContainer } from '@react-navigation/native';
import { UnauthenticatedNavigation } from 'navigations/unauthenticated';

export function UnauthenticatedRoutes() {
  return (
    <NavigationContainer>
      <UnauthenticatedNavigation />
    </NavigationContainer>
  );
}
