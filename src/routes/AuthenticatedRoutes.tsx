import { NavigationContainer } from '@react-navigation/native';
import { AuthenticatedNavigation } from 'navigations/authenticated';

export function AuthenticatedRoutes() {
  return (
    <NavigationContainer>
      <AuthenticatedNavigation />
    </NavigationContainer>
  );
}
