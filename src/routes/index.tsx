import { useAuth } from 'hooks/useAuth';

import { AuthenticatedRoutes } from './AuthenticatedRoutes';
import { UnauthenticatedRoutes } from './UnauthenticatedRoutes';

export function AppRouteManager() {
  const { isAuthenticated } = useAuth();

  return isAuthenticated ? <AuthenticatedRoutes /> : <UnauthenticatedRoutes />;
}
