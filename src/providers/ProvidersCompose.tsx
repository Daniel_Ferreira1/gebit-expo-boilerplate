/**
 * Compose provider
 * @param provider The provider to compose
 * @param props Component props to pass to provider
 * @returns Provider and props
 * @example
 * ```tsx
 *   provider(AuthContext.Provider);
 *   provider<ComponentProps>(ComponentWithProps, { myProp: 'myValue' });
 * ```
 */
export function provider<T>(provider: React.ComponentType<T>, props: T = {} as T) {
  return [provider, props];
}

/**
 * Compose multiple providers
 * @param providers Array of providers to compose
 * @param children Children to wrap with composed providers
 * @returns Composed providers
 * @example
 * ```tsx
 *  <ProviderComposer
 *   providers={[
 *    provider(AuthContext.Provider),
 *    provider<QueryClientProviderProps>(QueryClientProvider, { client: queryClient }),
 *    // provider(AnotherContext),
 *   ]}
 *  >
 *    {children}
 *  </ProviderComposer>
 * ```
 */
export const ProviderComposer = ({
  providers,
  children,
}: {
  providers: (any | React.ComponentType<any>)[];
  children: any;
}) => {
  for (let i = providers.length - 1; i >= 0; --i) {
    const [Provider, props] = providers[i];
    children = <Provider {...props}>{children}</Provider>;
  }
  return children;
};
