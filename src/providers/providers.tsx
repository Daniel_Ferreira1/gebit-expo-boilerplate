import { QueryClientProvider, QueryClientProviderProps } from '@tanstack/react-query';
import AuthContext from 'contexts/AuthContext';
import { queryClient } from 'lib/react-query';

import { provider, ProviderComposer } from './ProvidersCompose';

export function Providers({ children }: { children: React.ReactNode }) {
  return (
    <ProviderComposer
      providers={[
        provider(AuthContext.Provider),
        provider<QueryClientProviderProps>(QueryClientProvider, { client: queryClient }),
        // provider(AnotherContext),
      ]}
    >
      {children}
    </ProviderComposer>
  );
}
