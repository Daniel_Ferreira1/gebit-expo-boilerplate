import { useAuth } from 'hooks/useAuth';
import { Button, SafeAreaView, Text, View } from 'react-native';

export function LoginScreen() {
  const { signIn } = useAuth();

  const handlePressLogin = () => {
    signIn({
      token: 'token',
      user: {},
    });
  };

  return (
    <SafeAreaView
      style={{
        backgroundColor: '#fff',
        height: '100%',
      }}
    >
      <View
        style={{
          alignItems: 'center',
          flex: 1,
          justifyContent: 'center',
          gap: 10,
        }}
      >
        <Text style={{ fontSize: 16, fontWeight: 'bold' }}>Login</Text>
        <Text style={{ fontSize: 24, fontWeight: 'bold' }}>Gebit Boilerplate Expo</Text>
        <Button onPress={handlePressLogin} title='Login' />
      </View>
    </SafeAreaView>
  );
}
