import { UserType } from 'interfaces/commom/UserTypes';
import React, { createContext, useMemo, useReducer } from 'react';

interface InitialState {
  isAuthenticated: boolean;
  user: UserType;
  token: string;
}

interface AuthContextData extends InitialState {
  signIn: (data: { user: UserType; token: string }) => void;
  signOut: () => void;
  handleUpdateUser: (user: UserType) => void;
}

export const AuthContext = createContext<AuthContextData>({} as AuthContextData);

type Action =
  | {
      type: 'SIGN_IN';
      user: UserType;
      token: string;
    }
  | { type: 'SIGN_OUT' };

const reducer = (state: InitialState, action: Action) => {
  switch (action.type) {
    case 'SIGN_IN':
      return {
        ...state,
        user: action.user,
        token: action.token,
        isAuthenticated: true,
      };
    case 'SIGN_OUT':
      return {
        ...state,
        token: '',
        user: {} as UserType,
        isAuthenticated: false,
      };
    default:
      return state;
  }
};

const initialState = {
  isAuthenticated: false,
  user: {} as UserType,
  token: '',
};

function AuthProvider({ children }: { children: React.ReactNode }) {
  const [state, dispatch] = useReducer(reducer, initialState);

  const signIn = (data: { user: UserType; token: string }) => {
    dispatch({
      type: 'SIGN_IN',
      user: data.user,
      token: data.token,
    });
  };

  const signOut = async () => {
    // await AsyncStorage.multiRemove(["token", "user"])
    // Colocar nome das chaves que serão removidas
    dispatch({ type: 'SIGN_OUT' });
  };

  const handleUpdateUser = async (user: UserType) => {
    // await AsyncStorage.setItem("user", JSON.stringify(user));
  };

  const values = useMemo(
    () => ({
      ...state,
      signIn,
      signOut,
      handleUpdateUser,
    }),
    [state],
  );

  return <AuthContext.Provider value={values}>{children}</AuthContext.Provider>;
}

export default {
  Context: AuthContext,
  Provider: AuthProvider,
};
