import { BottomTabNavigationOptions } from '@react-navigation/bottom-tabs';

export interface TabBarIconProps {
  focused: boolean;
  color: string;
  size: number;
}

export interface IBottomTabsNavigation<T> {
  name: T;
  component: React.ComponentType<any>;
  tabBarIcon: (props: TabBarIconProps) => React.ReactElement;
  options?: BottomTabNavigationOptions;
  navigationKey?: string;
}
