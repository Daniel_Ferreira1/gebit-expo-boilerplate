import { NavigationProp } from '@react-navigation/native';

export enum AuthenticatedRoutes {
  Home = 'Home',
}

export interface AuthenticatedNavigationPages {
  [AuthenticatedRoutes.Home]: undefined;
  // [AuthenticatedRoutes.ProductDetails]: { id: string }; // Exemplo de passagem de parâmetro
}

export type AuthenticatedNavigationProps = NavigationProp<AuthenticatedNavigationPages>;
