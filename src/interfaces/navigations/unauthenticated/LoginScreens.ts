import { NavigationProp } from '@react-navigation/native';

export enum LoginScreens {
  Login = 'Login',
}

export interface LoginScreensRoutesNavigationPages {
  [LoginScreens.Login]: undefined;
}
export type LoginRoutesNavigationProps = NavigationProp<LoginScreensRoutesNavigationPages>;
