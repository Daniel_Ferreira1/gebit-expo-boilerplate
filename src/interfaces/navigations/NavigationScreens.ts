export interface INavigationScreens<T> {
  name: T;
  component: React.ComponentType<any>;
}
