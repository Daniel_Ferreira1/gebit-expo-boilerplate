import axios from 'axios';
// import AsyncStorage from "@react-native-async-storage/async-storage";

export const api = axios.create({
  baseURL: 'http://localhost:3000',
});

// Se quiser adicionar um token no header de todas as requisições
// api.interceptors.request.use(
//   async (config) => {
//     const token = await AsyncStorage.getItem(AppVariants.STORAGE_TOKEN_KEY);
//     if (token) {
//       config.headers["Authorization"] = `Bearer ${token}`;
//     }
//     return config;
//   },
//   (error) => Promise.reject(error)
// );
