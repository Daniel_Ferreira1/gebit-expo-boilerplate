import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { INavigationScreens } from 'interfaces/navigations/NavigationScreens';
import { AuthenticatedRoutes } from 'interfaces/navigations/authenticated/AuthenticatedScreens';
import { HomeScreen } from 'screens/authenticated/Home';

const Stack = createNativeStackNavigator();

const navigationScreens: INavigationScreens<AuthenticatedRoutes>[] = [
  {
    name: AuthenticatedRoutes.Home,
    component: HomeScreen,
  },
];

export function AuthenticatedNavigation() {
  return (
    <Stack.Navigator>
      {navigationScreens.map((item, index) => (
        <Stack.Screen
          key={`${item.name}${String(index + 1)}`}
          name={item.name}
          component={item.component}
          options={{
            headerShown: false,
          }}
        />
      ))}
    </Stack.Navigator>
  );
}
