import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { INavigationScreens } from 'interfaces/navigations/NavigationScreens';
import { LoginScreens } from 'interfaces/navigations/unauthenticated/LoginScreens';
import { LoginScreen } from 'screens/unauthenticated/LoginScreen';

const Stack = createNativeStackNavigator();

const navigationScreens: INavigationScreens<LoginScreens>[] = [
  {
    name: LoginScreens.Login,
    component: LoginScreen,
  },
];

export function UnauthenticatedNavigation() {
  return (
    <Stack.Navigator>
      {navigationScreens.map((item, index) => (
        <Stack.Screen
          key={`${item.name}${String(index + 1)}`}
          name={item.name}
          component={item.component}
          options={{
            headerShown: false,
          }}
        />
      ))}
    </Stack.Navigator>
  );
}
